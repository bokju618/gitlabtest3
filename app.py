# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup
from threading import Thread

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
import matplotlib.pyplot as plt
import numpy as np

from slack.web.classes import extract_json
from slack.web.classes.blocks import *

# OAuth & Permissions로 들어가서
# Bot User OAuth Access Token을 복사하여 문자열로 붙여넣습니다
SLACK_TOKEN = "xoxb-685329889623-689065782742-bHvpHOFjFoOdcJjFLWggc6dT"
# Basic Information으로 들어가서
# Signing Secret 옆의 Show를 클릭한 다음, 복사하여 문자열로 붙여넣습니다
SLACK_SIGNING_SECRET = "f34e96e105acd07543fc5fd94095dbe1"

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

#slack_web_client.chat_postMessage(    channel="#random",     text="*진한 글씨* _비스듬한 글씨_ ~취소선~ `코드`" )
#slack_web_client.chat_postMessage(     channel="#random",     text="<https://ssafy.elice.io|엘리스>는 정말 최고야!" )
#slack_web_client.chat_postMessage(     channel="#random",     text="<@U12345678> 님이 나를 보셨어!\n날 발할라로 데려가실 거야!")


# block1 = SectionBlock(
#     text="Slack APIs allow you to integrate complex services with Slack to go beyond the integrations we provide out of the box."
# )
#
# block2 = SectionBlock(
#     fields = ["Slack APIs allow you to integrate complex services with Slack to go beyond the integrations we provide out of the box.",
#              "Method, Description. apps.permissions.users.list, Returns list of user grants and corresponding scopes this app has on a team. apps.permissions.users.request"]
# )
#
# block3 = ImageBlock(
#     image_url="https://steemitimages.com/p/GTCx6Wb8cEhNmtUWxw4ojMPwTYxR7SvqNiYZjFFYF43atpbD4mRG?format=match&mode=fit&width=640",
#     alt_text="이미지가 안 보일 때 대신 표시할 텍스트"
# )
#
# my_blocks = [block1, block2, block3]
# slack_web_client.chat_postMessage(     channel="#random",    blocks = extract_json(my_blocks) )
count = 0
client_msg_id = ''
event_ts = 0
# 크롤링 함수 구현하기
def _crawl_music_chart(text):
    print("비교 차트 그리기")

    plt.xlabel("person")
    plt.ylabel("grade")

    # Python
    plt.plot(["a", "b", "c", "d", "e"], [100, 97, 46, 28, 84])

    # Java
    plt.plot(["a", "b", "c", "d", "e"], [58,28, 37, 83, 22])

    plt.savefig("image.png", format="png")

    threading.Thread()
    file_name ="image.png"
    slack_web_client.files_upload(channels="#random", file=file_name)
    # slack_web_client.chat_postMessage(     channel="#random",    blocks = extract_json(my_blocks) )

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    # print(event_data)
    # print(event_data['event']['client_msg_id'])
    #
    # global client_msg_id
    # recent_client_msg_id = event_data['event']['client_msg_id']
    # global event_ts
    # recent_event_ts = event_data['event']['event_ts']
    #
    # if (client_msg_id == recent_client_msg_id):
    #     # if event_ts != 0:
    #     #     if abs(recent_event_ts - event_ts) < 3000 :
    #             return
    #
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    #
    # _crawl_music_chart(text)
    # # message = _crawl_music_chart(text)
    # # slack_web_client.chat_postMessage(
    # #     channel=channel,
    # #     text=keywords
    # # )
    # #slack_web_client.chat_postMessage(channel="#random", blocks=extract_json(message))
    # client_msg_id = recent_client_msg_id
    # event_ts = recent_event_ts
    _crawl_music_chart(text)

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"

if __name__ == '__main__':
    app.run('127.0.0.1', port=5000)
